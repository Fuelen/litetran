About LiteTran
--------------
LiteTran is simple text translate utility.


Features
--------
- User-friendly interface
- Translation of selected text in popup (Ctrl+Shift+T by default)
- No annoying windows - only small popup with translation
- Text pronunciation
- 37 Languages supported
- It's possible to choose only necessary languages to show in interface
- Tray icon let you access LiteTran window and settings quickly


Platforms
---------
LiteTran works fine on Linux, requires Qt >= 5.2
Windows version also available, but have limited support of translate popup.


Build Depdendencies
-------------------
cmake >= 2.8.10
qt core, gui, multimedia, x11extras >= 5.2


Contribute
----------
Please send pull request or create issue.

Other notes
-----------

This project has been moved from https://github.com/flareguner/litetran/